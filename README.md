# My PyTorch Tutorial

Doing some tutorials to try out PyTorch.

## Fundamentals
* [Linear Regression](https://medium.com/@ally_20818/pytorch-101-linear-regression-with-pytorch-d2d22291c37d)
* [CNN](https://github.com/pytorch/examples/blob/master/mnist/main.py)
* [RNN](https://www.kaggle.com/mikebaik/simple-rnn-with-pytorch)
* [LSTM](https://pytorch.org/tutorials/intermediate/char_rnn_classification_tutorial.html)

## Reinforcement Learning
* DQN
* DRQN
* PPO

## Temporal Networks
* Attention
* [Transformer Network](https://github.com/jadore801120/attention-is-all-you-need-pytorch)
* [Recurrent Attention](https://github.com/kevinzakka/recurrent-visual-attention)
* [TCN](https://github.com/locuslab/TCN)

# Other RL Topics
* Intrinsic Motivation
* GAN models

